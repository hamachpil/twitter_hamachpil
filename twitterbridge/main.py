"""Main function for the Hamachpil Twitter bot"""
from os import environ as env
import logging
from time import sleep, time
import urllib3
import tweepy
from .utils import twitter_authenticate, twitter_url
from .settings import config, accounts, toot_template, redis, huey
from .tasks import toot_tweet

# create logger
logger = logging.getLogger()
log_level = env.get('LOG_LEVEL') or 'DEBUG'
logger.setLevel(log_level)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

formatter = logging.Formatter("%(asctime)s;%(levelname)s;%(message)s",
                              "%Y-%m-%d %H:%M:%S")
# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)

logging.debug("Loading config file")

# Map the accounts to a dictionary of accounts
twitter_account_map = {int(value.get('twitter_id')):value
                       for key,value in accounts.items() if key not in ('DEFAULT', 'all')}
logging.debug(f"twitter_account_map: {twitter_account_map}")



def tweet_filter(status):
    """Decide if the status (tweet) should be processed or not"""

    # First get the original tweeter
    if hasattr(status, 'retweet_status'):
        user_id = status.retweet_status.user.id
    else:
        user_id = status.user.id

    # Now look to see if the tweeter is in the account map
    if user_id in twitter_account_map:
        acct = twitter_account_map[user_id]
        url = (acct, status.id)
        logging.info(f"Toot {url} should be posted from {acct['twitter_username']}")
        return acct

    # If they're not the direct user, this is probably a reply or a notification
    in_reply_to_user_id = getattr(status, 'in_reply_to_user_id', None)
    if in_reply_to_user_id in twitter_account_map:
        url = twitter_url(status.author.screen_name, status.id)
        replied_url = twitter_url(status.in_reply_to_screen_name,
                                  status.in_reply_to_status_id)
        logging.info(f"Skipping reply tweet {url} in reply to {replied_url}")
        return False

    # It could be a retweet
    if hasattr(status, 'retweeted_status'):
        if status.retweeted_status.user.id in twitter_account_map:
            url = twitter_url(status.author.screen_name, status.id)
            orig_url = twitter_url(status.retweeted_status.user.screen_name,
                                   status.retweeted_status.id)
            logging.info(f"Skipping retweet notification tweet {url} of {orig_url}")
            return False

    # It's likely a user mention. Check for all mentions
    for user in status.entities['user_mentions']:
        if user['id'] in twitter_account_map:
            url = twitter_url(status.author.screen_name, status.id)
            logging.info(f"Skipping notification {url} to {user['screen_name']}")
            return False

    # Finally we are in an unknown cause and the tweet should be examined
    url = twitter_url(status.user.screen_name, status.id)
    logging.error(f"TWEET UNKNOWN STATUS: {url}")
    return False


# Find a better name
class TwitterStream(tweepy.Stream):

    def on_keep_alive(self):
        logging.info("Keep-Alive Recieved")

    def on_status(self, status):
        logging.debug(f"Processing {status.id}")
        if acct := tweet_filter(status):
            logging.debug(
                f"Prepping to toot tweet {status.id} from {acct['mastodon_username']}")
            toot_tweet(acct, status)
            redis.set('lastseen', status.id)
        else:
            redis.set('lastseen', status.id)

    def on_error(self, status_code):
        if status_code == 420:
            # We have exceeded our rate limit.
            #
            # In the future we can use a more complex/exponential
            # backoff strategy but 10 mnutes seems reasonable for our small bot
            mins = 10
            logging.error(f"EXCEEDED OUR RATE LIMIT. Sleeping {mins} minutes")
            sleep(60 * mins)
        else:
            logging.warning(f"Recieved {status_code} from Twitter. Status code not handled")

    def on_timeout(self):
        logging.error("TIMEOUT RECIEVED. RECONNECTING...")
        return True


def main():
    logging.info("Logging into Twitter")
    tw_auth = twitter_authenticate(config['twitter'])
    # Now we must get the list of following. It appears we can't directly use the follower list :(
    api = tweepy.API(tw_auth, wait_on_rate_limit=True)
    follow_list = [i.id_str for i in api.get_friends()]
    logging.info(f"Following {follow_list}")

    lastseen = redis.get('lastseen')
    logging.debug(f"lastseen loaded as : {lastseen}")
    if not lastseen:
        logging.info("No lastseen in DB. Not catching up.")
    else:
        lastseen = int(lastseen)
        logging.info(f"Catching up since tweet {lastseen}")
        # Catch up to old tweets we may have missed
        for status in api.home_timeline(since_id=lastseen,
                                        tweet_mode="extended"):
            logging.debug(f"Processing {status.id}")
            if acct := tweet_filter(status):
                logging.debug(
                    f"Prepping to toot tweet {status.id} from {acct['mastodon_username']}")
                toot_tweet(acct, status)
                redis.set('lastseen', int(status.id))
            else:
                redis.set('lastseen', int(status.id))

    # Now read any new tweets
    #
    # We need to grab the credentials again
    cfg = config['twitter']
    consumer_key = cfg.get('consumer_key')
    consumer_secret = cfg.get('consumer_secret')
    access_token = cfg.get('access_token')
    access_token_secret = cfg.get('access_token_secret')
    
    stream = TwitterStream(consumer_key, consumer_secret, access_token, access_token_secret)
    
    while True:
        try:
            stream.filter(follow=follow_list)
        except urllib3.exceptions.ProtocolError:
            # This is an acceptable exception that appears to occur
            # every so often. Ignore and retry
            logging.error("Recieved an HTTP protocol error. Retrying.")
        # Allow other exceptions to raise higher and cause a failure

if __name__ == '__main__':
    main()
