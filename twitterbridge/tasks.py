from html import unescape
from time import time
from datetime import datetime, timedelta
import logging
import mastodon
from .utils import twitter_url
from .settings import config, huey, redis, toot_template

def tw_expand_urls(text, urls):
    for url in urls:
        text = text.replace(url['url'], url['expanded_url'])
    return text

def format_toot(tweet):
    """Format a tweet into a toot"""

    retweeted = False

    # See if this is a retweet of an existing tweet
    # We prefer the extended tweet full text
    if hasattr(tweet, 'retweeted_status') and \
       hasattr(tweet.retweeted_status, 'extended_tweet'):
        logging.debug(f"Found retweet with extended_tweet on {tweet.id_str}")
        logging.debug(f"Setting status to retweeted_status.extended_status")
        status = tweet.retweeted_status
        retweeted = status.id_str
        text = status.extended_status['full_text']
        
    elif hasattr(tweet, 'extended_tweet'):
        logging.debug(f"Found extended tweet on {tweet.id_str}")
        status = tweet.extended_tweet
        text = status['full_text']
    else:
        # This happens if we're processing from the home_timeline
        # api. We don't have the extended_status
        if hasattr(tweet, 'full_text'):
            logging.debug(f"Found raw status on {tweet.id_str}")
            status = tweet
            text = status.full_text
        else:
            # Fallback and grab the short text. This should not
            # happen
            logging.debug("Unable to grab extended status for {tweet.id_str}")
            status = tweet
            text = status.text

    if hasattr(status, 'entities'):
        # Expand the URLs you can
        text = tw_expand_urls(text, status.entities.get('urls', []))
        text = text.strip()
        # Strip off the media tags
        for med in status.entities.get('media', []):
            text = text.replace(med['url'], '')
        text = text.strip()

        if hasattr(status, 'extended_status'):
            for med in status.extended_status.entries.get('media', []):
                text = text.replace(med['url'], '')
        text = text.strip()
    
    # Twitter may be putting the url of the tweet as the last word in
    # the tweet. If we see it, remove it since we already credit the
    # tweet elsewhere
    lastword = text.split(' ')[-1]
    if lastword.startswith('https://t.co'):
        text.replace(lastword, '')
        text = text.strip()

    # If this tweet is in reply to another, then check if we have
    # it. If we don't, then make the in_reply_to explicit
    
    in_reply_to_status_id = getattr(tweet, 'in_reply_to_status_id', None)
    if in_reply_to_status_id:
        # Try to see if we have this twitter reply in the db
        toot = redis.get(tweet.id)
        # Only create in_reply_to for tweets that *don't* have a
        # corresponding toot
        if not toot:
            in_reply_to_url = twitter_url(in_reply_to_status_id,
                                          tweet.in_reply_to_screen_name)
            in_reply_to = (tweet.in_reply_to_screen_name, in_reply_to_url)
    else:
        in_reply_to = None

    # Unescape html codes
    text = unescape(text)

    return toot_template.render(tweet=tweet, retweeted=retweeted,
                                in_reply_to=in_reply_to, text=text)

@huey.task(retries=99, retry_delay=60)
def toot_tweet(account, tweet, dry_run=False):
    """Send tweet to be tooted"""
    tweet_url = twitter_url(tweet.user.screen_name, tweet.id)
    # Toot Template
    logging.info(
        f"TOOT {tweet_url} to account {account['mastodon_username']}")

    in_reply_to = getattr(tweet, 'in_reply_to_status_id', None)

    text = format_toot(tweet)

    if dry_run:
        logging.debug("PRINT {tweet.id} from {account['mastodon_username']}")
        print(f"{text}")
    else:
        api_base_url = config['mastodon']['api_base_url']
        auth_token = account['mastodon_token']
        mstdn = mastodon.Mastodon(api_base_url=api_base_url,
                                  access_token=auth_token)
        toot = None
        if in_reply_to:
            # Try to see if we have this twitter reply in the db
            toot = redis.get(in_reply_to)
        if toot:
            in_reply_toot_id = toot['toot_id']
            toot = mstdn.status_post(text, in_reply_to_id = in_reply_toot_id,
                                     idempotency_key=tweet.id_str)
        else:
            toot = mstdn.status_post(text, idempotency_key=tweet.id_str)

        tweet_url = twitter_url(tweet.user.screen_name, tweet.id)
        logging.info(f"Tooted {toot.url} as mirror of {tweet_url}")
        #
        # Store this toot in the DB
        redis.hmset(tweet.id, {'tweet_id': tweet.id,
                              'toot_id': toot.id,
                              'acct_id': toot.account.id,
                              'acct_name': toot.account.acct,
                              'time': int(time())})
        ttl = timedelta(days=7)
        redis.expire(tweet.id, ttl)

