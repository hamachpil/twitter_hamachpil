from os import environ as env
from dotenv import load_dotenv
from redis import Redis
from huey import RedisHuey
from jinja2 import Template
from .utils import load_config, load_accounts

load_dotenv()

REDIS_HOST = env.get('REDIS_HOST', 'localhost')
CONFIG_FILE = env.get('CONFIG_FILE', 'config.ini')
ACCOUNTS_FILE = env.get('ACCOUNTS_FILE', 'accounts.ini')
DB_FILE = env.get('DB_FILE', 'db.json')

config = load_config(CONFIG_FILE)
accounts = load_accounts(ACCOUNTS_FILE)
redis = Redis(host=REDIS_HOST)

huey = RedisHuey('twitterbridge-huey', host=REDIS_HOST)

toot_template = Template("""
    {% if retweeted %}
    RT @{{ tweet.retweeted_status.author.screen_name }} https://twitter.com/{{ tweet.retweeted_status.author.screen_name }}/status/{{tweet.retweeted_status.id}}
    {% elif in_reply_to %}
    in reply to: @{in_reply_to[1]} {in_reply_to[0]}
    {% endif %}

    {{ text }}

    Original Tweet: https://twitter.com/{{tweet.user.screen_name}}/status/{{tweet.id}}
    """)
