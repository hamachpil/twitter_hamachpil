"""Util functions for Hamacpil twitter->mastodon bridge"""

import configparser
import tweepy

def load_config(config_file):
    """Load a configparser ini file and return configfile object"""
    cfg = configparser.ConfigParser()
    with open(config_file, 'r') as fd:
        cfg.read_file(fd)
        return cfg

def load_accounts(accounts_file):
    """Load accounts ini file and return as a configparser object"""
    accounts = configparser.ConfigParser()
    with open(accounts_file, 'r') as fd:
        accounts.read_file(fd)
        return accounts


def twitter_authenticate(cfg):
    """Log into twitter and return a twitter authentication object

    Args:
       cfg (dict): dict containing consumer_key, consumer_secret, access_token,
                   and access_token_secret

    Returns:
        tweepy.OAuthHandler: A pre-authorized OAuth handler
    """
    consumer_key = cfg.get('consumer_key')
    consumer_secret = cfg.get('consumer_secret')
    access_token = cfg.get('access_token')
    access_token_secret = cfg.get('access_token_secret')
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    return auth

def twitter_url(screen_name, status_id):
    return f"https://twitter.com/{screen_name}/status/{status_id}"
