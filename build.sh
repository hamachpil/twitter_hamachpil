set -ex # SET THE FOLLOWING VARIABLES
# image name

if  [ -z "$1" ]; then
    IMAGE=$(poetry version|awk '{ print $1 ;}')
else
    IMAGE=$1
fi

if [ -z "$2" ]; then
   VERSION=$(poetry version|awk '{ print $2 ;}')
else
   VERSION=$2
fi

docker build -t ${IMAGE}:latest -t ${IMAGE}:${VERSION} .
