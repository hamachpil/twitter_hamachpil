Ultimately the Hamachpil Twitter bot is feasible to use outside of Hamacpil, but it is not a general purpose project. The code is provided primarily for educational purposes and the setup/instructions are provided for future maintainers.

# Setup

Srtting up the bot for use is not difficult, but nor is it trivial.

## Twitter Signup

The bot requires a number of steps before it can be run. First, it requies a Twitter API account, specifically the OAuth 1 (not OAuth 2) version, as the commands must be run from a single Twitter account context.

There are many guides online on how to generate a Twitter bot account, such as this one:  https://blog.hubspot.com/website/how-to-make-a-twitter-bot

## Mastodon Setup

The bot requires a Mastodon instance and the script must be registered with the API for that as well.

Setting up Mastodon is beyond the scope of this guide, but once you have the Mastodon instance, you can use this guide to generate the API keys necessary for the bot to function: https://shkspr.mobi/blog/2018/08/easy-guide-to-building-mastodon-bots/

## Environment variables

| Variable      | Meaning                   | Default      |
|---------------|---------------------------|--------------|
| CONFIG_FILE   | Location of config file   | config.ini   |
| ACCOUNTS_FILE | Location of accounts file | accounts.ini |
| LASTSEEN_FILE | Location of lastseen      | lastseen.txt |
| DB_FILE       | Location of Database file | db.json      |
|               |                           |              |

It should be noted that the location of the files in a default Docker installation becomes prepended with `/data`, ie `/data/config.ini`. This is due to the Docker build use of the `env.docker` being placed as `.env` in the run environment

It should not be necessary to modify the environment variables unless you wish to move the files.

It's also notable that the configurastion and data files live in the same directory. This could be modified if desired.

## config.ini

The `config.ini` file contains the core Twitter and Mastodon configuration. A default config file is provided as `sample_config.ini`.

## accounts.ini

The `accounts.ini` file contains information about each Hamachpil account, including their Twitter id/username and  the Mastodon name and authentication details.

This file should generally not be edited by hand.

## lastseen.txt

The `lastseen.txt` file contains the most recently seen Tweet status ID as well as the time that status ID was seen, in Unix time.

## db.json

The `db.json` file is the database of recently sent toots (Mastodon status updates), along with the Twitter status IDs they represent. This is used to allow the program to create threaded posts.

# Docker Compose

An example of a `docker-compose.yaml` configuration is included in the repository and may be customized (as it is in the production instance).

# Running

The Docker `entrypoint.sh` file has three primary commands, `run`, `admin` and `shell`.

The `shell` command executes a bash shell.

The `run` command runs the twitter->mastodon bot.

Only one instance of the bot should run at a given time.

The `admin` command runs one of the adminstrative commands and is primarily used for account maintence (ie creating a new Hamachpil account).

By default, the `run` command is assumed.

# Account Creation

Unfortunately there does not appear to be a remote API to create Mastodon accounts, meaning that the process for account creation is somewhat manual In the future we may be able to automate this process further, but as this process is fairly infrequent, the process for account maintence is only semi-automated for now.

To create an account, use the admin.py command, such as

```
./admin.py create_account TWITTER-USERNAME
```

The admin app will instruct you on what commands to run from there.

# License

This project is available under the Apache 2.0 License, though the libraries it depends are available under their own licenes.
