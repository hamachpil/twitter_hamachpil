#!/bin/sh
set -ex

IMAGE=poetry version|awk '{ print $1 ;}'
VERSION=$(poetry version|awk '{ print $2 ;}')

docker save ${IMAGE}:latest | gzip > ../${IMAGE}-latest.tar.gz
