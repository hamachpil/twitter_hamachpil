FROM python:3.10

RUN mkdir /app
RUN mkdir /data
RUN touch /data/lastseen.txt
RUN touch /data/db.json

COPY . /app
COPY env.docker /app/.env
COPY config_ini.sample /data/config.ini

WORKDIR /app
ENV PYTHONPATH=${PYTHONPATH}:${PWD}

RUN pip3 install poetry
RUN poetry config virtualenvs.create false
# This should hopefully be fixed in the future, but for now we need to disable --no-dev so we can have platformdirs
#RUN poetry install --no-dev
RUN poetry install

