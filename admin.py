#!/usr/bin/env python


import click
from utils import load_config, twitter_authenticate, load_accounts
import tweepy
from mastodon import Mastodon
from pprint import pprint
import configparser
import subprocess
from robohash import Robohash
from shutil import copyfile
from tempfile import NamedTemporaryFile
from pathlib import Path
import requests
from tempfile import mkstemp, gettempdir
import sys

@click.group()
def account():
    pass

@account.command()
@click.argument('twitter_screen_name')
def show_twitter_info(twitter_screen_name):
    config = load_config()
    twitter_auth = twitter_authenticate(config['twitter'])
    api = tweepy.API(twitter_auth)
    user = api.get_user(screen_name=twitter_screen_name)
    print(f"Screen Name: {user.screen_name}")
    print(f"User ID: {user.id_str}")
    print(f"Name: {user.name}")
    print(f"Description: {user.description}")
    print(f"Profile image at {user.profile_image_url_https}")

@account.command()
@click.argument('twitter_screen_name')
def copy_from_twitter(twitter_screen_name):
    config = load_config()
    accounts = load_accounts(config.get('general', 'accounts'))
    #
    # Look for Mastodon username in config
    #
    mastodon_username = accounts.get(twitter_screen_name, 'mastodon_username')
    mastodon_token = accounts.get(twitter_screen_name, 'mastodon_token')
    mstdn = Mastodon(api_base_url=config.get('mastodon', 'api_base_url'),
                     access_token=accounts.get(twitter_screen_name, 'mastodon_token'))

    twitter_auth = twitter_authenticate(config['twitter'])
    twitter_api = tweepy.API(twitter_auth)
    twitter_user = twitter_api.get_user(screen_name=twitter_screen_name)

    # Now map the Twitter info to Mastodon
    d = {'display_name': f"@{twitter_user.screen_name}@twitter",
         'note': f"Via Twitter: {twitter_user.description} ",
         'bot': True,
         'discoverable': True,
         'fields': (('url', twitter_user.url),),
         }
    mstdn.account_update_credentials(**d)
    click.echo("Done")

@account.command()
@click.argument('twitter_screen_name')
@click.argument('password')
def mastodon_login(twitter_screen_name, password):
    config = load_config()
    accounts = load_accounts(config.get('general', 'accounts'))
    account = accounts[twitter_screen_name]

    mstdn = Mastodon(
        client_id = config['mastodon']['client_key'],
        client_secret = config['mastodon']['client_secret'],
        api_base_url = config['mastodon']['api_base_url'],
    )
    token = mstdn.log_in(account['mastodon_email'],
                         password,
                         scopes=["read", "write"])
    account['mastodon_password'] = password
    account['mastodon_token'] = token
    print(f"mastodon_token={token}")
    accountsfile = config.get('general', 'accounts')
    copyfile(accountsfile, f"{accountsfile}.old")
    with open(accountsfile, 'w') as acctfile:
        accounts.write(acctfile)

@account.command()
def show_following():
    config = load_config()
    accounts = load_accounts(config.get('general', 'accounts'))
    twitter_auth = twitter_authenticate(config['twitter'])
    api = tweepy.API(twitter_auth)
    # Grab the followers list, sort by username, then check if there's
    # a corresponding entry in accounts
    # Twitter calls people you follow your "friends"
    friends = api.friends()
    # Map the friends to a dict
    followers = {f.screen_name:f for f in friends}
    for f in sorted(followers.keys()):
        if f in accounts:
            print(f)
        else:
            print(f"{f} (Not in Accounts)")

@account.command()
@click.argument('twitter_screen_name')
def create_bot_avatar(twitter_screen_name):
    config = load_config()
    accounts = load_accounts(config.get('general', 'accounts'))
    account = accounts[twitter_screen_name]
    hash = f"@{account['mastodon_username']}@{config['mastodon']['api_base_url']}"
    rh = Robohash(hash)
    rh.assemble(roboset='set1')
    avatar_filename = NamedTemporaryFile(suffix='.png').name
    with open(avatar_filename, 'wb') as fd:
        rh.img.save(fd, format="png")
    mstdn = Mastodon(api_base_url=config['mastodon']['api_base_url'],
                     access_token=account['mastodon_token'])
    mstdn.account_update_credentials(avatar=avatar_filename)

@account.command()
@click.argument('twitter_screen_name')
def create_twitter_avatar(twitter_screen_name):
    config = load_config()
    accounts = load_accounts(config.get('general', 'accounts'))
    account = accounts[twitter_screen_name]

    twitter_auth = twitter_authenticate(config['twitter'])
    twitter_api = tweepy.API(twitter_auth)
    twitter_user = twitter_api.get_user(screen_name=twitter_screen_name)

    image_url = twitter_user.profile_image_url
    # We want to work with the original image, which means we need to remove _normal from the url
    image_url = image_url.replace('_normal', '')
    # This is fragile but works for Twitter images
    image_filename = image_url.rsplit('/', 1)[1]

    # Download the image
    downloaded_path = Path(gettempdir(), image_filename)
    if not downloaded_path.exists():
        response = requests.get(image_url)
        response.raise_for_status()
        if response.status_code == 200:
            with open(downloaded_path, 'wb') as fd:
                fd.write(response.content)

    # Resize the image
    base, extension = str(downloaded_path).rsplit('.', 1)
    resized_path = Path(gettempdir(), f"{base}-resized.{extension}")
    if not resized_path.exists():
        subprocess.run(['convert', downloaded_path, '-resize', '400x400>',
                        resized_path])

    # Add the bot watermark
    #
    # Currently we only support the 'repeater' badge, but in the future we
    # may support others, as well as the Hamachpil logo itself on the image.
    
    avatar_path = Path(gettempdir(), f"{twitter_screen_name}-avatar.png")
    if not avatar_path.exists():
        subprocess.run(['convert', resized_path, 'badges/recycle.png',
                        # The gravity center is only applied to the geometry
                        '-gravity', 'center', '-geometry', '+80+80',
                        #  Compose the images on top of each other
                        '-compose', 'over',
                        '-background', 'black',
                        '-composite',
                        avatar_path], check=True)

    # Set the profile to the new image
    # Upload the new image to Mastodon
    image_data = open(avatar_path, 'rb').read()
    mstdn = Mastodon(api_base_url=config['mastodon']['api_base_url'],
                     access_token=account['mastodon_token'])
    mstdn.account_update_credentials(avatar=image_data,
                                     avatar_mime_type='image/png')
    
@account.command()
@click.argument('twitter_screen_name')
@click.option('-f', '--force')
def create_account(twitter_screen_name, force=False):
    mastodon_email = f"admin+{twitter_screen_name.lower()}_tw@hamachpil.net"
    mastodon_username = f"{twitter_screen_name}_tw"
    config = load_config()
    accounts = load_accounts(config.get('general', 'accounts'))
    if not force and twitter_screen_name in accounts:
        click.echo(f"{twitter_screen_name} already in accounts. To override, use force")
        click.Abort()
    # Get the Twitter User object for this user
    twitter_auth = twitter_authenticate(config['twitter'])
    twitter_api = tweepy.API(twitter_auth)
    twitter_user = twitter_api.get_user(screen_name=twitter_screen_name)

    d = {
        'twitter_username': twitter_screen_name,
        'twitter_id': twitter_user.id,
        'mastodon_username': mastodon_username,
        'mastodon_email': mastodon_email,
        'mastodon_password': ''
        }
    print(f"docker-compose run --rm web bin/tootctl accounts create {mastodon_username} --email {mastodon_email} --confirmed\n")
    
    print(f"docker-compose run --rm web bin/tootctl accounts modify {mastodon_username} --approve\n")

    print(f"./admin.py finalize-mastodon {twitter_screen_name} <password>")

    # ./admin.py copy-from-twitter username
    
    # Create the Mastodon user
    #
    # Log into Mastodon as the admin
    
    mst_cfg = config['mastodon']
    mstdn = Mastodon(api_base_url=mst_cfg['api_base_url'],
                     access_token=mst_cfg['access_token'])
    # Now create the new account
#    new_access_token  = mstdn.create_account(mastodon_username, password, mastodon_email, agreement=True)
        
    # Save the credentials to the config file
    accounts[twitter_screen_name] = {
        'twitter_username': twitter_screen_name,
        'twitter_id': twitter_user.id,
        'mastodon_username': mastodon_username,
        'mastodon_email': mastodon_email,
        }
    # Save the accounts file
    accountsfile = config.get('general', 'accounts')
    copyfile(accountsfile, f"{accountsfile}.old")
    with open(config.get('general', 'accounts'), 'w') as acctfile:
        accounts.write(acctfile)

@account.command()
@click.pass_context
@click.argument('twitter_screen_name')
@click.argument('password')
def finalize_mastodon(ctx, twitter_screen_name, password):
    print(twitter_screen_name)
    print(password)
    ctx.invoke(mastodon_login, twitter_screen_name=twitter_screen_name,
               password=password)
    ctx.invoke(copy_from_twitter, twitter_screen_name=twitter_screen_name)
    ctx.invoke(create_twitter_avatar, twitter_screen_name=twitter_screen_name)


if __name__ == '__main__':
    account()
