#!/bin/sh
set -e

echo "Run the Twitter->Mastodon bot"

if  [ -z "$1" ] || [ "$1" = 'run' ]; then
    python /app/app.py
elif ["$1" = 'shell' ]; then
    exec /bin/bash
elif ["$1" = 'admin' ]; then
    python /app/admin.py echo "${@:2}"
else
        exec "$@"
fi
